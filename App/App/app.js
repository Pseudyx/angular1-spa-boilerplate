﻿var backOffice = {
    appModule: angular.module("appModule", [
          'ui.router'
        , 'ngResource'
        , 'ui.bootstrap'
        , 'ngProgress'
        , 'checklist-model'
        , 'sharedModule'
        , 'homeModule'
        , 'catalogueModule'
    ]).
        factory('configSvc', function (ngProgress) {
            var brand = {
                label: "Shop"
            };

            var endpoints = {
                apiUri: 'http://localhost:55339' //prompt('apiUri')
            };

            var navigation = {
                mainNav: [
                    { label: "Catalogue", state: "catalogue" }
                ],
                footerNav: {}
            }

            ngProgress.color('#428bca');

            return {
                brand: brand,
                endpoints: endpoints,
                navigation: navigation
            };
        }).
        config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider) {
            $locationProvider.html5Mode(true);
            $urlRouterProvider.otherwise('/');
            $stateProvider.
                state('home', {
                    url: '/',
                    templateUrl: 'App/components/home/index.html',
                    controller: 'homeCtrl'
                }).
                state('catalogue', {
                    url: '/catalogue',
                    templateUrl: 'App/components/catalogue/index.html',
                    controller: 'catalogueCtrl',
                    resolve: {
                        catalogueList: function (ngProgress) {
                            ngProgress.start();
                            var varList = [{ "Name": "test1", "Code": "000", "Description": "this is a test", "Price": "1.00" },
                            { "Name": "test2", "Code": "001", "Description": "this is a test", "Price": "1.00" }];
                            return varList;
                        }
                    }
                });
        }]),
    sharedModule: angular.module('sharedModule', []),
    homeModule: angular.module('homeModule', []),
    catalogueModule: angular.module('catalogueModule', [])
}