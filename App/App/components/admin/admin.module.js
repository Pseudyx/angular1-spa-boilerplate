﻿backOffice.adminModule.
    config([
        '$stateProvider', 
        function ($stateProvider) {
            $stateProvider.
                state('admin_templates', {
                    url: '/admin/templates',
                    templateUrl: 'App/components/admin/templates.html',
                    controller: 'templateLstCtrl',
                    resolve: {
                        templateList: function(adminSvc) {
                            return adminSvc.GetTemplates();
                        }
                    }
                }).
                state('admin_scrum', {
                    url: '/admin/scrum/:templateId',
                    templateUrl: 'App/components/admin/scrum.html',
                    controller: 'templateScrumCtrl',
                    resolve: {
                        scrumModel: function ($stateParams, adminSvc) {
                            return adminSvc.BreakdownViewModel($stateParams.templateId);
                        }
                    }
                });
        }
    ]).
    factory('adminSvc', [
    '$http', '$q', 'configSvc',
    function($http, $q, configSvc) {
        return {
            GetTemplates: function() {
                var deferred = $q.defer();
                $http({ method: 'GET', url: configSvc.endpoints.apiUri + '/Admin/Templates' }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            AddTemplate: function(template) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: configSvc.endpoints.apiUri + '/Admin/AddTemplate', data: template }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            DeleteTemplate: function(id) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: configSvc.endpoints.apiUri + '/Admin/TemplateDelete/' + id }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            BreakdownViewModel: function(id) {
                var deferred = $q.defer();
                $http({ method: 'GET', url: configSvc.endpoints.apiUri + '/Admin/Scrum/' + id }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            AddUnit: function(unitVm) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: configSvc.endpoints.apiUri + '/Admin/UnitAdd', data: unitVm }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            UpdateUnit: function(breakdown) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: configSvc.endpoints.apiUri + '/Admin/UnitUpdate', data: breakdown }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            DeleteUnit: function(id) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: configSvc.endpoints.apiUri + '/Admin/UnitDelete/' + id }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            AddPhase: function(phaseVm) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: configSvc.endpoints.apiUri + '/Admin/PhaseAdd', data: phaseVm }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            DeletePhase: function(id) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: configSvc.endpoints.apiUri + '/Admin/PhaseDelete/' + id }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            UpdateRate: function(complexity) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: configSvc.endpoints.apiUri + '/Admin/RateUpdate', data: complexity }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            UpdateFormula: function(complexity) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: configSvc.endpoints.apiUri + '/Admin/FormulaUpdate', data: complexity }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }

        }
    }
]);

backOffice.adminModule.controller('adminCtrl', [
    '$scope',
    function ($scope) {
    }
]);

backOffice.adminModule.controller('templateLstCtrl', [
    '$scope', 'adminSvc', '$state', '$modal', 'templateList',
    function ($scope, adminSvc, $state, $modal, templateList) {
        $scope.template = {}
        $scope.template.list = templateList;
        $scope.getTemplates = function() {
            adminSvc.GetTemplates().then(function (templateList) { $scope.template.list = templateList; }, function () { alert('error while fetching data from server'); });
        }
        $scope.template.doClick = function (item) {
            $state.go('admin_scrum', { templateId: item.ProjectTemplateID });
        };
        $scope.add = function(template) {
            adminSvc.AddTemplate(template).then(function() {
                $scope.getTemplates();
            }, function() {
                alert('error while adding template at server');
            });
        };
        $scope.deleteTemplate = function (templateId, $event) {
            $event.stopPropagation();
            adminSvc.DeleteTemplate(templateId).then(function () {
                $scope.getTemplates();
            }, function() {
                alert('error while deleting Template at server');
            });
            //confirm('Please confrim Delete Scrum Team', function() {
        };
        $scope.items = ['item1', 'item2', 'item3'];
        $scope.open = function (size) {

            var modalInstance = $modal.open({
                templateUrl: '_NewTemplate.html',
                controller: 'newTemplateInstanceCtrl',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (template) {
                $scope.add(template);
            }, function () {
                //on cancel modal
            });
        };
    }
]);

backOffice.adminModule.controller('newTemplateInstanceCtrl', [
    '$scope', '$modalInstance', 'items',
    function($scope, $modalInstance, items) {
        $scope.ok = function() {
            $modalInstance.close($scope.template);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);

backOffice.adminModule.controller('templateScrumCtrl', [
    '$scope', '$stateParams', 'adminSvc', 'scrumModel',
    function ($scope, $stateParams, adminSvc, scrumModel) {
        $scope.template = {};

        if (scrumModel) {
            $scope.template.Title = scrumModel.ProjectTemplate.Title;
            $scope.scrumTeam =
            {
                ID: scrumModel.ProjectTemplate.ProjectTemplateID,
                Unit: ''
            }
            $scope.scrumPhase =
            {
                ID: scrumModel.ProjectTemplate.ProjectTemplateID,
                Phase: ''
            }
            $scope.scrum = scrumModel;
        }

        $scope.getScrum = function () {
            adminSvc.BreakdownViewModel($stateParams.templateId).then(function(scrum) {
                $scope.template.Title = scrum.ProjectTemplate.Title;
                $scope.scrumTeam =
                {
                    ID: scrum.ProjectTemplate.ProjectTemplateID,
                    Unit: ''
                }
                $scope.scrumPhase =
                {
                    ID: scrum.ProjectTemplate.ProjectTemplateID,
                    Phase: ''
                }
                $scope.scrum = scrum;
            }, function () { alert('error while fetching data from server'); });
        }
        $scope.UpdateUnit = function(breakdown) {
            adminSvc.UpdateUnit(breakdown).then(function () {
               //todo:change textbox colour
            }, function () {
                alert('error while adding template at server');
            });
        }
        $scope.blnAddScrum = false;
        $scope.tgglAddScrum = function () { $scope.blnAddScrum = !$scope.blnAddScrum; }
        $scope.AddScrum = function (phaseVm) {
            adminSvc.AddPhase(phaseVm).then(function () {
                $scope.getScrum();
            }, function () {
                alert('error while adding Scrum at server');
            });
            $scope.tgglAddScrum();
        }
        $scope.deleteScrum = function (phaseId) {
            adminSvc.DeletePhase(phaseId).then(function () {
                $scope.getScrum();
            }, function () {
                alert('error while deleting Scrum at server');
            });
            //confirm('Please confrim Delete Scrum Team', function() {
        }
        $scope.blnAddScrumTeam = false;
        $scope.tgglAddScrumTeam = function () { $scope.blnAddScrumTeam = !$scope.blnAddScrumTeam; }
        $scope.AddScrumTeam = function(unitVm) {
            adminSvc.AddUnit(unitVm).then(function () {
                $scope.getScrum();
            }, function () {
                alert('error while adding Scrum Team at server');
            });
            $scope.tgglAddScrumTeam();
        }
        $scope.deleteScrumTeam = function (unitId) {
            adminSvc.DeleteUnit(unitId).then(function () {
                $scope.getScrum();
            }, function () {
                alert('error while deleting Scrum team at server');
            });
            //confirm('Please confrim Delete Scrum Team', function() {
        }
        $scope.UpdateRate = function (complexity) {
            adminSvc.UpdateRate(complexity).then(function () {
                //todo:change textbox colour
            }, function () {
                alert('error while adding template at server');
            });
        }
        $scope.UpdateFormula = function (complexity) {
            adminSvc.UpdateFormula(complexity).then(function () {
                //todo:change textbox colour
            }, function () {
                alert('error while adding template at server');
            });
        }
    }
]);