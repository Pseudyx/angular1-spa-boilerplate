﻿backOffice.catalogueModule.
    config([
        '$stateProvider',
        function($stateProvider) {

        }
    ]);

backOffice.catalogueModule.controller('catalogueCtrl', [
    '$scope', '$state', '$modal', 'ngProgress', 'catalogueList',
    function ($scope, $state, $modal, ngProgress, catalogueList) {
        if (catalogueList) {
            ngProgress.complete();
            $scope.catalogue = {}
            $scope.catalogue.list = catalogueList;
        }
    }
]);