﻿backOffice.homeModule.factory('homeSvc', ['$http', '$q', 'configSvc',
    function ($http, $q, configSvc) {
        return {
            GetIntro: function () {
                var deferred = $q.defer();
                $http({ method: 'GET', url: configSvc.endpoints.apiUri + '/Home/GetIntroText' }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }
        }
    }
]);

backOffice.homeModule.controller('homeCtrl', [
    '$scope', 'homeSvc', 'configSvc',
    function ($scope, homeSvc, configSvc) {
        $scope.brand = configSvc.brand;
    }
]);