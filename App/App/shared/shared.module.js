﻿backOffice.sharedModule.controller("layoutCtrl", [
    '$scope', 'configSvc',
    function ($scope, configSvc) {
        $scope.brand = configSvc.brand;
        $scope.copyright = new Date().getFullYear() + " " + $scope.brand.label;
        $scope.Nav = configSvc.navigation;
    }
]);